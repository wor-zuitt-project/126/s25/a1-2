/*


Activity: 


In Postman, create a new collection named s25-a1 and add the following 
request to that collection:

1. GET all request to jsonplaceholder's todo resource
2. GET specific request for todo id 199 to jsonplaceholder's todo resource
3. POST request to jsonplaceholder's todo resource
4. PUT request for todo id 123 to jsonplaceholder's todo resource
5. DELETE request for todo id 7 to jsonplaceholder's todo resource

Make sure to test each request to see if it is working, and if it is working, make usre
to propertly save the request. 


*/


const http = require("http");

const server = http.createServer(function (request, response){
	if(request.url === "/items" && request.method === "GET"){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Items retreived from database.')
	} 

})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)






